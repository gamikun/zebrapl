Zebra Programming Language II (ZPL) for Python
============

*This is a Zebra ZPL II communication protocol*

List of supported barcodes:
*Code 128

This is the simplest way to make it work
    
    from zebra.net import Printer
    from zebra.doc import Document, Label, Barcode, BARCODE_128

    d = Document()
    d.add(Label(data=u'Hello World!'))
    d.add(Barcode(data=u'1234443',type=Barcode.CODE128))

    printer = Printer('192.168.1.1')
    printer.open()
    printer.printdoc(d, darkness=9)
    printer.close()

	print d.tostring()