from zebrapl.net import Printer
from zebrapl.doc import Document, Label, Barcode, BARCODE_128

d = Document()
d.add(Label(data=u'Hello World!'))
d.add(Barcode(data=u'1234443',type=BARCODE_128))

printer = Printer('192.168.1.1')
printer.open()
printer.printdoc(d, darkness=9)
printer.close()

print d.tostring()