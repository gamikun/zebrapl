# -*- coding: utf-8 -*-

"""
Zebra Programming Language II SDK for Python
"""

__title__ = 'zebrapl'
__version__ = '0.37.2'
__build__ = 0x02
__author__ = 'Gamaliel Espinoza'
